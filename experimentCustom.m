%
% This is DE algorithm for BBOB benchmark
% 
more off; % to get immediate output inpo Octave
warning('off')
% Free memory if was error before free at the end
if exist('observer', 'var') == 1
    cocoObserverFree(observer);
    clearvars observer;
end

if exist('suite', 'var') == 1
    cocoSuiteFree(suite);
    clearvars suite;
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%
% Experiment Parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%
BUDGET_MULTIPLIER = 2000; % algorithm runs for BUDGET_MULTIPLIER*dimension funevals

%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare Experiment    %
%%%%%%%%%%%%%%%%%%%%%%%%%

% choose a test suite and a matching logger, for
% example one of the following:
%
% bbob               24 unconstrained noiseless single-objective functions
% bbob-biobj         55 unconstrained noiseless bi-objective functions
% [bbob-biobj-ext     92 unconstrained noiseless bi-objective functions]
% bbob-largescale    24 unconstrained noiseless single-objective functions in large dimensions
% [bbob-constrained* 48 constrained noiseless single-objective functions]
% bbob-mixint        24 unconstrained noiseless single-objective functions with mixed-integer variables
% bbob-biobj-mixint  92 unconstrained noiseless bi-objective functions with mixed-integer variables
%
% Suites with a star are partly implemented but not yet fully supported.
%
suite_name = 'bbob';
observer_name = 'bbob';
observer_options = strcat('result_folder: init-R-SHADE_', ...
    suite_name, ...
    [' algorithm_name: Test_'...
    ' algorithm_info: Test_']);

% initialize suite and observer with default options,
% to change the default, see 
% http://numbbo.github.io/coco-doc/C/#suite-parameters and
% http://numbbo.github.io/coco-doc/C/#observer-parameters
% for details.
% 
suite = cocoSuite(suite_name, ...
'', ...
'dimensions: 2,3,5,10,20,40');

% suite = cocoSuite(suite_name, ...
% '', ...
% 'dimensions: 2');

% suite = cocoSuite(suite_name, ...
% '', ...
% 'dimensions: 2 function_indices: 6');
%instances: 77'

% suite = cocoSuite(suite_name, ...
% '', ...
% 'dimensions: 5');

% suite = cocoSuite(suite_name, ...
% '', ...
% 'dimensions:2 function_indices: 15');

observer = cocoObserver(observer_name, observer_options);

% set log level depending on how much output you want to see, e.g. 'warning'
% for fewer output than 'info'.
cocoSetLogLevel('info');

% keep track of problem dimension and #funevals to print timing information:
printeddim = 1;
doneEvalsAfter = 0; % summed function evaluations for a single problem
doneEvalsTotal = 0; % summed function evaluations per dimension
printstring = '\n'; % store strings to be printed until experiment is finished

%%%%%%%%%%%%%%%%%%%%%%%%
% Run Experiment        %
%%%%%%%%%%%%%%%%%%%%%%%%

% Fixed random generator
rng('default')
%rng(123)
while true
    % get next problem and dimension from the chosen suite:
    problem = cocoSuiteGetNextProblem(suite, observer);
    if ~cocoProblemIsValid(problem)
        break;
    end
    dimension = cocoProblemGetDimension(problem);
    
    % printing
    if printeddim < dimension
      if printeddim > 1
        elapsedtime = toc;
        printstring = strcat(printstring, ...
            sprintf('   COCO TIMING: dimension %d finished in %e seconds/evaluation\n', ...
            printeddim, elapsedtime/double(doneEvalsTotal)));
        tic;
      end
      doneEvalsTotal = 0;
      printeddim = dimension;
      tic;
    end  
    
    % start algorithm with remaining number of function evaluations:
    opt_lqrshade(problem,...
        cocoProblemGetSmallestValuesOfInterest(problem),...
        cocoProblemGetLargestValuesOfInterest(problem),...
        cocoProblemGetNumberOfIntegerVariables(problem),...
        cocoProblemGetNumberOfConstraints(problem),...
        BUDGET_MULTIPLIER*dimension);
    

end


elapsedtime = toc;
printstring = strcat(printstring, ...
    sprintf('   COCO TIMING: dimension %d finished in %e seconds/evaluation\n', ...
    printeddim, elapsedtime/double(doneEvalsTotal)));
fprintf(printstring);


% clear memory but also variables to prevent double free
cocoObserverFree(observer);
clearvars observer;
cocoSuiteFree(suite);
clearvars suite;

    