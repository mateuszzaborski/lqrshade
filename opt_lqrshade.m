function opt_lqrshade (problem, lower_bounds, upper_bounds, num_integer_vars, num_constraints, budget, fileID)

format long;
format compact;

disp(cocoProblemGetName(problem))

%%  Parameter settings for L-SHADE
D = cocoProblemGetDimension(problem);
max_nfes = budget;

max_region = 5;
min_region = -5;
lu = [min_region * ones(1, D); max_region * ones(1, D)];

p_best_rate = 0.09;
arc_rate = 0.12;
memory_size = 11;
NP = round(3.96 * D);
%NP = 2*D + 2;

initial_sf = 0.38;
initial_cr = 0.94;

% Restart
use_classic_restart = true;
restart_eps_x = 1e-12;
restart_eps_y = 1e-12;
evals_stop = 500;


% LQ Archive params
df_lin = D + 1;
df_quad = 2*D + 1;
df_full = 1 + 2*D + D*(D-1)/2;
NQ = max(2*df_full, NP);
%queue_size = 8;
%n_true_evals = NP * 0.25;

Nsurr = 1;
use_model = false;
model_init = true;
thres_model_rho = -1.0;
thres_model_rho_smooth = 0.1;
thres_model_R2 = 0.0;
thres_model_R2_smooth = 1.0;

%% Custom experiments params
log_all_surrogates = false;
save_evaluations = true;

if log_all_surrogates == true   
    fileID = fopen(strcat("log/", cocoProblemGetName(problem), ".csv"), 'W');
    fprintf(fileID,'%s%s%s%s%s\n', "Evals", ";", "Type", ";", "Tau");
end


%% Main loop with restarts
nfes = 0;
restart_cnt = 0;
while nfes < max_nfes
    
    %% LQ Archive
    q = NaN(NQ,D);
    f_q = NaN(NQ,1);
    n_M = 0;
    rho_new = 1;
    rho = 1;
    R2_new = 1;
    R2 = 1;
    
    % Restart init
    best_solution = Inf;
    no_update_evals_cnt = 0;
    
    %% Initial population - normal
    if model_init == false
        %pop = lower_bounds + rand(NP, D) .* (upper_bounds - lower_bounds);
        pop = double((upper_bounds - lower_bounds).*lhsdesign(single(NP),single(D)) + lower_bounds);    
        
        fitness = NaN(NP, 1);
        for i = 1:NP
            fitness(i) = cocoEvaluateFunction(problem, pop(i,:));
            nfes = nfes + 1;
            %% Update queue
            % Shift Queue
            q(2:end,:) = q(1:end-1,:);
            f_q(2:end) = f_q(1:end-1);
            % Increment queue elements
            n_M = min(n_M + 1, NQ);
            % Add to queue
            q(1,:) = pop(i,:);
            f_q(1) = fitness(i);
        end
    end
    %% Initial population - model
    if model_init == true
        %pop = lower_bounds + rand(NP, D) .* (upper_bounds - lower_bounds);
        pop = double((upper_bounds - lower_bounds).*lhsdesign(single(NP),single(D)) + lower_bounds);    
        
        fitness = NaN(NP, 1);
        for i = 1:NP
            %% Linear possible
            if i == df_lin + 1
                %% Build model
                X = [pop(1:df_lin,:), ones(df_lin,1)];
                Y = fitness(1:df_lin);
                X2 = X'*X;
                b = X2\X'*Y;
                %% Designate optimum
                pop(i,:) = repmat(-5, D, 1);
                idx = b(1:D) < 0;
                pop(i,idx) = 5;
                fitness(i) = cocoEvaluateFunction(problem, pop(i,:));

            %% Quadratic possible    
            elseif i == df_quad + 1
                %% Build model
                X = [pop(1:df_quad,:) .* pop(1:df_quad,:), pop(1:df_quad,:), ones(df_quad,1)];
                Y = fitness(1:df_quad);
                X2 = X'*X;
                b = X2\X'*Y;
                %% Designate optimum
                X_peak = -b(D+1:end-1) ./ b(1:D)/2;
                y_sub_peaks = NaN(D, 1);
                y_sub_left = NaN(D, 1);
                y_sub_right = NaN(D, 1);
                for j = 1:D
                    if abs(b(j)) > 1e-12
                        y_sub_peaks(j) = sum([X_peak(j) * X_peak(j) , X_peak(j)] .* [b(j) , b(D+j)]);
                    else
                        y_sub_peaks(j) = Inf;
                    end                
                     y_sub_left(j) = sum([25 , -5] .* [b(j) , b(D+j)]);
                     y_sub_right(j) = sum([25 , 5] .* [b(j) , b(D+j)]);
                end
                pop(i,:) = X_peak;
                idx_left = y_sub_left < y_sub_peaks & y_sub_left <= y_sub_right;
                pop(i,idx_left) = -5;
                idx_right = y_sub_right < y_sub_peaks & y_sub_right < y_sub_left;
                pop(i,idx_right) = 5;
                fitness(i) = cocoEvaluateFunction(problem, pop(i,:));
                
            %% Normal evaluation   
            else
                fitness(i) = cocoEvaluateFunction(problem, pop(i,:));
            end
            
            nfes = nfes + 1;
            
            %% Update queue - conditional
            if (~ismembertol(pop(i,:), q, 1e-12, 'ByRows',true)) & (~ismembertol(fitness(i), f_q, 1e-12, 'ByRows',true))
                % Shift Queue
                q(2:end,:) = q(1:end-1,:);
                f_q(2:end) = f_q(1:end-1);
                % Increment queue elements
                n_M = min(n_M + 1, NQ);
                % Add to queue
                q(1,:) = pop(i,:);
                f_q(1) = fitness(i);    
            end
            %% Sort queue
            [f_q_sorted, f_q_sorted_idx] = sort(f_q);
            f_q = f_q_sorted;
            q = q(f_q_sorted_idx,:);                 
        end       
    end
    memory_sf = initial_sf .* ones(memory_size, 1);
    memory_cr = initial_cr.* ones(memory_size, 1);
    memory_pos = 1;
    
    archive.NP = arc_rate * NP; % the maximum size of the archive
    archive.pop = zeros(0, D); % the solutions stored in te archive
    archive.funvalues = zeros(0, 1); % the function value of the archived solutions
    
    %% main loop
    while nfes < max_nfes
        nfes_start = nfes;
        %% Memory
        [temp_fit, sorted_index] = sort(fitness, 'ascend');
        
        mem_rand_index = ceil(memory_size * rand(NP, 1));
        mu_sf = memory_sf(mem_rand_index);
        mu_cr = memory_cr(mem_rand_index);
        
        %% for generating crossover rate
        cr = normrnd(mu_cr, 0.1);
        term_pos = find(mu_cr == -1);
        cr(term_pos) = 0;
        cr = min(cr, 1);
        cr = max(cr, 0);
        
        %% Mutation
        % for generating scaling factor
        sf = mu_sf + 0.1 * tan(pi * (rand(NP, Nsurr) - 0.5));
        
        for i = 1:Nsurr
            pos = find(sf(:,i) <= 0);
            while ~ isempty(pos)
                sf(pos,i) = mu_sf(pos) + 0.1 * tan(pi * (rand(length(pos), 1) - 0.5));
                pos = find(sf(:,i) <= 0);
            end
        end
        
        sf = min(sf, 1);
        r0 = [1 : NP];
        popAll = [pop; archive.pop];
        
        vi = NaN(NP, D, Nsurr);
        for i = 1:Nsurr
            [r1, r2] = gnR1R2mod(NP, size(popAll, 1), r0);
            
            pNP = max(single(round(p_best_rate * NP)), 2); %% choose at least two best solutions
            randindex = ceil(rand(1, NP) .* pNP); %% select from [1, 2, 3, ..., pNP]
            randindex = max(1, randindex); %% to avoid the problem that rand = 0 and thus ceil(rand) = 0
            pbest = pop(sorted_index(randindex), :); %% randomly choose one of the top 100p% solutions
            
            %%
            sf_i = sf(:,i);
            vi(:,:, i) = pop + sf_i(:, ones(1, D)) .* (pbest - pop + pop(r1, :) - popAll(r2, :));
            vi(:,:, i) = boundConstraint(vi(:,:, i), pop, lu);
        end
        
        %% Crossover
        mask = rand(NP, D) > cr(:, ones(1, D)); % mask is used to indicate which elements of ui comes from the parent
        rows = (1 : NP)';
        %cols = floor(rand(pop_size, 1) * single(problem_size))+1; % choose one position where the element of ui doesn't come from the parent
        cols =  randi([1 D],NP,1); % choose one position where the element of ui doesn't come from the parent
        jrand = sub2ind([NP D], rows, cols);
        mask(jrand) = false;
        ui = vi;
        for i = 1:Nsurr
            ui_i = ui(:,:,i);
            ui_i(mask) = pop(mask);
            ui(:,:, i) = ui_i;
        end
        
        %% Meta-model
        if n_M < df_lin
            ui =  ui(:,:, 1);
        else
            %% Linear model
            if n_M >= df_lin && n_M < df_quad
                %% Build model
                X = [q(1:n_M,:), ones(n_M,1)];
                Y = f_q(1:n_M);
                X2 = X'*X;
                b = X2\X'*Y;
                %% Designate current surrogate value
                M_ui = NaN(NP, Nsurr);
                % TODO improve performance
                for i = 1:Nsurr
                    for j = 1:NP
                        X_surr = [ui(j,:,i), 1];
                        M_ui(j,i) = sum(b .* X_surr');
                    end
                end
            end
            %% Quadratic model
            if n_M >= df_quad && n_M < df_full
                %% Build model
                X = [q(1:n_M,:) .* q(1:n_M,:), q(1:n_M,:), ones(n_M,1)];
                Y = f_q(1:n_M);
                X2 = X'*X;
                b = X2\X'*Y;
                %% Designate current surrogate value
                M_ui = NaN(NP, Nsurr);
                % TODO improve performance
                for i = 1:Nsurr
                    for j = 1:NP
                        X_surr = [ui(j,:,i) .* ui(j,:,i), ui(j,:,i), 1];
                        M_ui(j,i) = sum(b .* X_surr');
                    end
                end
            end
            %% Full model
            if n_M >= df_full
                %% Build model
                X = [NaN(n_M, df_full-1), ones(n_M,1)];
                X(:, 1:df_quad-1) = [q(1:n_M,:) .* q(1:n_M, :), q(1:n_M, :)];
                col_idx = df_quad;
                for i = 1:D
                    for j = (i+1):D
                        X(:, col_idx) = q(1:n_M,i) .* q(1:n_M,j);
                        col_idx = col_idx + 1;
                    end
                end
                Y = f_q(1:n_M);
                X2 = X'*X;
                b = X2\X'*Y;
                %% R2 calculation
                Yhat = b'*X';
                SStot = sum((Y-mean(Y)) .* (Y-mean(Y)));
                SSres = sum((Y - Yhat') .* (Y - Yhat'));
                R2_new = 1 - SSres ./ SStot;
                %% Designate current surrogate value
                M_ui = NaN(NP, Nsurr);
                % TODO improve performance
                for i = 1:Nsurr
                    for j = 1:NP
                        X_surr = [NaN(1, df_full-1), ones(1,1)];
                        X_surr(1:df_quad-1) = [ui(j,:,i) .* ui(j,:,i), ui(j,:,i)];
                        col_idx = df_quad;
                        for k = 1:D
                            for m = (k+1):D
                                X_surr(col_idx) = ui(j,k,i) .* ui(j,m,i);
                                col_idx = col_idx + 1;
                            end
                        end
                        M_ui(j,i) = sum(b .* X_surr');
                    end
                end
            end
            %%   
        end
        %% R2 smoothing
        R2 = (thres_model_R2_smooth*R2_new) + (1-thres_model_R2_smooth)*R2;
               
        %% Choose best ui using surrogate M-value
        if  use_model && rho >= thres_model_rho && R2 >= thres_model_R2
            [best_surr, best_surr_idx] = min(M_ui,[],2);
            ui_best = NaN(NP, D);
            for i = 1:NP
                ui_best(i,:) = ui(i,:, best_surr_idx(i));
            end
            ui = ui_best;
            % Final sf
            sf_copy = NaN(NP);
            for i = 1:NP
                sf_copy(i) = sf(i, best_surr_idx(i));
            end
            sf = sf_copy;
            % Surrogate fitness
            surr_fitness = best_surr;
        else
            ui_best = NaN(NP, D);
            for i = 1:NP
                ui_best(i,:) = ui(i,:, 1);
            end
            ui = ui_best;
            % Final sf
            sf = sf(:,1);  
            % Surrogate fitness
            surr_fitness = M_ui(:,1);
        end
        
       
        
        
        %% Offspring evaluation
        children_fitness = zeros(NP, 1);
        for i = 1:size(ui,1)
            children_fitness(i) = cocoEvaluateFunction(problem, ui(i,:));
            nfes = nfes + 1;           
            if n_M < NQ
                %% Update queue
                % Shift Queue
                q(2:end,:) = q(1:end-1,:);
                f_q(2:end) = f_q(1:end-1);
                % Increment queue elements
                n_M = min(n_M + 1, NQ);
                % Add to queue
                q(1,:) = ui(i,:);
                f_q(1) = children_fitness(i);              
            else
                %% Update queue - conditional
                if (children_fitness(i)) < f_q(NQ) & (~ismembertol(ui(i,:), q, 1e-12, 'ByRows',true)) & (~ismembertol(children_fitness(i), f_q, 1e-12, 'ByRows',true))
                    q(NQ,:) = ui(i,:);
                    f_q(NQ) = children_fitness(i);
                end               
            end  
            %% Sort queue
            [f_q_sorted, f_q_sorted_idx] = sort(f_q);
            f_q = f_q_sorted;
            q = q(f_q_sorted_idx,:);
        end
        
        %% Model tau-kendall
        if (log_all_surrogates == true) %& (cocoProblemFinalTargetHit(problem) == false)
            fprintf(fileID, '%d%s%s%s%.8f\n', nfes, ";", "o", ";", rho_new);
        end
        rho_new = corr(children_fitness, surr_fitness,'type','Kendall');
        if (log_all_surrogates == true) %& (cocoProblemFinalTargetHit(problem) == false)
            fprintf(fileID, '%d%s%s%s%.8f\n', nfes, ";", "n", ";", rho_new);
        end
        if isnan(rho_new) == true
            rho_new = 1.0;
        end
        rho = (thres_model_rho_smooth*rho_new) + (1-thres_model_rho_smooth)*rho;

        %         disp(corr(children_fitness, surr_fitness,'type','Kendall'));
        %         disp(rho);
        
        %% Selection
        dif = abs(fitness - children_fitness);
        I = (fitness > children_fitness);
        goodCR = cr(I == true);
        goodF = sf(I == true);
        dif_val = dif(I == true);
        
        archive = updateArchive(archive, pop(I == true, :), fitness(I == true));
        pop(I == true, :) = ui(I == true, :);
        fitness = min([fitness, children_fitness],[], 2);
        
        
        %% Update SF and CR memory
        num_success_params = numel(goodCR);
        
        if num_success_params > 0
            sum_dif = sum(dif_val);
            dif_val = dif_val / sum_dif;
            
            %% for updating the memory of scaling factor
            memory_sf(memory_pos) = (dif_val' * (goodF .^ 2)) / (dif_val' * goodF);
            
            %% for updating the memory of crossover rate
            if max(goodCR) == 0 || memory_cr(memory_pos)  == -1
                memory_cr(memory_pos)  = -1;
            else
                memory_cr(memory_pos) = (dif_val' * (goodCR .^ 2)) / (dif_val' * goodCR);
            end
            
            memory_pos = memory_pos + 1;
            if memory_pos > memory_size;  memory_pos = 1; end
        end
        
        
        %%
%         scatter(pop(:,1), pop(:,2), 50, fitness)
%         axis([-5 5 -5 5])
%         hold on                
%         scatter(q(:,1), q(:,2),100, f_q, 'x')        
               
        
        %fprintf('Exp: %f Shade: %f \n', measure_exp, measure_shade);
        
        %disp(min(fitness));
        
        
        %% Save evaluations
        if (save_evaluations == true) && (cocoProblemFinalTargetHit(problem) == 1)
            fprintf('Target hit!\n Used budget: %d\n', nfes)
            if log_all_surrogates == true
                fclose(fileID);
            end
            return
        end
        
        
        %% Restart scheme
        % Lack of updates counter
        pop_best_solution = min(fitness);
        if pop_best_solution < best_solution
            best_solution = pop_best_solution;
            no_update_evals_cnt = 0;
        else
            no_update_evals_cnt = no_update_evals_cnt + nfes - nfes_start;
        end
        
        cond_restart = false;
        if sum((max(pop) - min(pop)) < restart_eps_x * max(abs(pop))) > 0
            cond_restart = true;
        end
        if sum((max(fitness) - min(fitness)) < restart_eps_y * max(abs(fitness))) > 0
            cond_restart = true;
        end
        if no_update_evals_cnt >= evals_stop*D
            cond_restart = true;
        end
        % Skip restart
        if use_classic_restart == false
            cond_restart = false;
        end
        if cond_restart == true
            fprintf('Restart condition! Used budget: %d\n', nfes)
            restart_cnt = restart_cnt + 1;
            break;
        end
        %fprintf('Pop size: %d \n', pop_size)
    end
end
if log_all_surrogates == true
    fclose(fileID);
end

end

